
<?php

return [
/**
 * Max limit of Everyday.
 */
 'max_limit' =>'9',
 /**
  * Quiz Keyword
  */
 'keyword'  =>'RT',
 /**
  * Quiz shortcode
  */
 'shortcode' =>'16295',

 /**
  * operator name
  */
 'operator'  => '88018',

 /**
  * Quiz Name.
  */
 'q_name' =>'Robi Quiz Contest ',
];
