  <!-- navbar -->
    <nav>
      <div class="nav-wrapper">
        <a href="#" class="brand-logo left">
          <img class="responsive-img" src="{{ asset('public/images/logo.svg') }}" alt="">
        </a>
        <ul class="right">
          <li><a id="toggle-search" href="#"><i class="material-icons">search</i></a></li>
          <li><a class="waves-effect waves-light" href="#"><i class="material-icons">cloud_upload</i></a></li>
          <li><a class="dropdown-button waves-effect waves-light" data-activates="d1" href="#"><i class="material-icons">more_vert</i></a>
            <ul id='d1' class='dropdown-content'>
              <li><a href="index.html"><i class="material-icons left">home</i> Home</a></li>
              <li><a href="user_profile.html"><i class="material-icons left">perm_identity</i> Profile</a></li>
              <li><a href="account_settings.html"><i class="material-icons left">settings</i>Settings</a></li>
              <li><a href="#!"><i class="material-icons left">settings_power</i> Exit</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
    <!-- /navbar -->

<!-- tabmenu bar -->
    <div class="row z-depth-1 tabmenu">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="col-sm-3 col-md-3 col-lg-3">
          <center>
            <a class="active" href="{{ url('http://192.168.100.157/robitube/') }}"><img class="img_tab" src="{{ asset('public/images/icon/home.png') }}" alt=""></a>
          </center>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3">
          <center>
            <a href="{{ url('http://192.168.100.157/robitube/archive') }}"><img class="img_tab" src="{{ asset('public/images/icon/folder.png') }}" alt=""></a>
          </center>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3"">
          <center>
            <a href="{{ url('http://192.168.100.157/robitube/profile') }}" style="display: inline-block;"><img class="img_tab" src="{{ asset('public/images/icon/user.png') }}" alt=""></a>
          </center>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3"">
          <center>
            <a href="{{ url('/quiz') }}" style="display: inline-block;"><img class="img_tab" src="{{ asset('public/images/icon/quiz.png') }}" alt=""></a>
          </center>
        </div>
      </div>
    </div>
