<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>QUIZ :: @yield('title')</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('public/css/app.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('public/css/materialize.min.css')}}">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('public/css/materialdesignicons.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('public/css/style.css')}}">
</head>
<body style="overflow-x: hidden; ">
@include('layouts.nav')
<div class="container-fluid">
    <div class="col-xs-12">
        @yield('content')
    </div>
</div>
@include('layouts.footer')

</body>
</html>