<footer class="page-footer">
    <div class="row valign-wrapper">
        <div class="col s8">

            @if(Session::has('msisdn'))
                <small>To unsubscribe SMS <b>STOP RT</b> and send it to <b>16295</b>. <a class="faqterms" href="{{ url('faq') }}">FAQ</a> and <a class="faqterms" href="{{ url('/terms') }}">Terms</a></small>
            @else
                <small>To subscribe SMS <b>START RT</b> and send it to <b>16295</b>. <a class="faqterms" href="{{ url('faq') }}">FAQ</a> and <a class="faqterms" href="{{ url('/terms') }}">Terms</a></small>
            @endif
        </div>
        <div class="col s4">
            @if(Session::has('msisdn'))
                <?php
                  $subscriber = \App\SubscriptionModel::where('msisdn', '=', Session::get('msisdn'))->
                                where('status', '=', 1)->get();
                 ?>
                @if($subscriber->count() > 0)
                    <a class="btn btn-small white red-text right" href="{{ url('http://192.168.100.157/robitube/unsubscribe') }}">Unsubscribe</a>
                @else
                    <a class="btn btn-small white red-text right" href="{{ url('http://192.168.100.157/robitube/subscribe') }}">Subscribe</a>
                @endif
           @else
                <a class="btn btn-small white red-text right" href="{{ url('http://192.168.100.157/robitube/subscribe') }}">Subscribe</a>
            @endif
        </div>
    </div>
</footer>
<script src="{{asset('public/themes/default/js/jquery.min.js')}}"></script>
<script src="{{asset('public/js/materialize.min.js')}}"></script>
@if(isset($js))
    {{  $js }}
@endif
<script type="text/javascript">
    $(function() {

        var nav = $('.main').offset().top;
        var vh = $('.responsive-video').innerHeight() - $('.card-content').innerHeight();
        $('.main').css('min-height', $(window).height() - nav + 'px');
        $('.scrollable').css('max-height',  vh + 'px');
        $(".button-collapse").sideNav();
        $('.slider').slider({full_width: true});
        $('.modal-trigger').leanModal({
                    dismissible: true, // Modal can be dismissed by clicking outside of the modal
                    opacity: .5, // Opacity of modal background
                }
        );
        $('ul.tabs').tabs('select_tab', 'tab_id');

        // Toggle search
        $('a#toggle-search').click(function()
        {
            var search = $('div#search');
            console.log('test');

            search.is(":visible") ? search.slideUp() : search.slideDown(function()
            {
                search.find('input').focus();
            });
            return false;
        });

        $('a.likes').on('click', function(e) {
            var id = $(e.currentTarget).attr('data-target');
            var label = $(this);
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '{{ url('video/like') }}/' + id,
                success: function(data) {
                    if(data.success == true ) {
                        label.html('<svg class="svg" viewBox="0 0 24 24"><path d="M23,10C23,8.89 22.1,8 21,8H14.68L15.64,3.43C15.66,3.33 15.67,3.22 15.67,3.11C15.67,2.7 15.5,2.32 15.23,2.05L14.17,1L7.59,7.58C7.22,7.95 7,8.45 7,9V19A2,2 0 0,0 9,21H18C18.83,21 19.54,20.5 19.84,19.78L22.86,12.73C22.95,12.5 23,12.26 23,12V10.08L23,10M1,21H5V9H1V21Z" /></svg>' + data.likes);
                    }
                }
            });

        });

        //Dropdown Button
        $('.dropdown-button').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrain_width: true, // Does not change width of dropdown to that of the activator
                    hover: false, // Activate on hover
                    gutter: 0, // Spacing from edge
                    belowOrigin: true, // Displays dropdown below the button
                    alignment: 'middle' // Displays dropdown with edge aligned to the left of button
                }
        );

        $('select').material_select();

    });
</script>
@if(isset($extrajs))
    {{ $extrajs }}
@endif
