@extends('../layouts.master')
@section('title', 'Robi')
@section('content')
 
 		<div class="quiz-description">
               <div class="col-sm-12  col-md-6 col-md-offset-3">
              <div class="panel panel-primary">
                   <div class="panel-heading text-center">
                     <h5>Robi Quiz Contest.</h5>
                    </div>
                   

                    <div class="trm-n-con">
                    <p>Play Robi Tube quiz and win Exclusive Laptop (ZedBook W 2 in 1 Convertible Laptop with Free Wireless Mouse).
                    </p>
                         <p><b><u>Terms & Conditions</u></b></p>
                    <ul >
                         
                         <li>Each correct answer will carry 10 points.</li>
                         <li>No negative points for wrong answer.</li>
                         <li>Daily question limit is 10 (question will not carry forward in next days).</li>
                         <li>Players who will reach the benchmark points (1300) first will be declared as winners.</li>
                         <li>Winners will be notified by SMS.</li>
                         <li>Campaign duration: 15 days.</li>
                         <li>Data charge is applicable. No EXTRA charge for this quiz contest.</li>
                         <li>Per calendar day will be countable from 12PM to next 12 PM</li>
                         
                    </ul>
                    </div>
                <div class="panel-body text-center thanks">
                  <p>Let’s play to win Exclusive Laptop.<br> <b>GOOD LUCK!</b></p>
                     <a href="{{url('question')}}" class="btn btn-lg btn-info" role="button">Start the Quiz</a>
                   </div>
               </div>
          </div>       
          </div>

@stop
