@extends('../layouts.master')
@section('title', 'Question')

@section('content')
 		<div class="col-sm-12 col-md-6 col-md-offset-3" style="min-height: 450px;">
 		   <div class="panel panel-primary" style="">
		       <div class="panel-heading">
		        <h5 class="text-right"><b>Right: {{$score}}  Wrong: {{$wrong}}</b></h5>
		      	<h5 class="text-right"><b>Score: {{$score*10}}</b></h5>
		       </div>
			   <div class="panel-body">
		          @if($errors->any())
	                <div class="alert alert-danger fade in">
	                  <a href="#" class="close" data-dismiss="alert">&times;</a>
	                   <br><br>
	                      <ul>
	                        @foreach ($errors->all() as $error)
	                         <li><strong>{{ $error }}</strong></li>
	                         @endforeach
	                      </ul>
	                </div>
	              @endif 
		          <?php if($question) { ?>
				  <form  role="form" method="POST" action="{{url('ques_check')}}">
		            {{ csrf_field() }}
		            <input name="quiz_id" type="hidden" value="{{$question['id']}}">
		            <input name="answer" type="hidden" value="{{$question['answered']}}">
	                <h5>{{$todayPlayed+1}}.  {{$question['quiz']}}</h5>
	                <div class="form-group">
		                <div class="radio">
						  <label><input type="radio" name="optradio" value="A" required >{{$question['option_a']}}</label>
						</div>
						<div class="radio">
						  <label><input type="radio" name="optradio" value="B" required >{{$question['option_b']}}</label>
						</div>
						<div class="radio">
						  <label><input type="radio" name="optradio" value="C" required>{{$question['option_c']}}</label>
		                </div>
	                </div>
				    <input type="submit" class="btn btn-lg btn-warning n-btn" value="Next">
				  </form>
				  <?php } else{ ?>
				  
                    <h5>Your daily limit of 10 questions are over. Come tomorrow to continue the quiz.</h5>
                    <a href="{{url('/quiz')}}" class="btn btn-success" role="button">Go Home</a>
                   <?php } ?>
				 
			    </div>
			</div>
 		</div>
@stop