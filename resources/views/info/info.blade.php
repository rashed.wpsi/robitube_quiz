@extends('../layouts.master')
@section('title', 'Subscription Info')

@section('content')


 		<div class="col-sm-12  col-md-6 col-md-offset-3">
 		    <div class="panel panel-primary ">
	            <div class="panel-heading text-center">
	               <h5>Robi Quiz Contest.</h5>
	            </div>
	            <div class="panel-body"> 
                   <p>To subscribe, type START {{Config::get('custom.keyword')}} & send to {{Config::get('custom.shortcode')}}.After Successfully subscription please click on next the button</p>
                   <a href="{{url('rules')}}" class="btn btn-lg btn-info" role="button">Next</a>
	            </div>   
            </div>
 		</div>
 

@stop