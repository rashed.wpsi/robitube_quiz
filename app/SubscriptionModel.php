<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionModel extends Model
{
  /**
   * connection name
   */
   protected $connection='sqlsrv1';
  /**
   * table name
   * @var string
   */
  
   protected $table = 'subscribers';

   public function scopeSubscriber($query, $msisdn)
    {
        return $query->where('telco_inMobileNo', '=', $msisdn);
    }
   public function scopeStatus($query, $status)
    {
        if($status > 0) {
            return $query->where('subscription_enabled', '=', $status);
        }
    }
   public function scopeShortcode($query, $shortcode)
   {
     return $query->where('telco_inShortCode', '=', $shortcode);
   } 
   public function scopeKeyword($query, $keyword)
    {
        
      return $query->where('msg_keyword', '=', $keyword);
        
    }
}
