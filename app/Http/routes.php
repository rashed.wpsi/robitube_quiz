<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



 Route::group(['middleware' => 'subscription'], function () {
 	//Route::get('/','HomeController@index');
	Route::get('/quiz','HomeController@rules');
	Route::get('question','QuestionController@index');
	Route::post('ques_check','QuestionController@ques_check');

 });
