<?php

namespace App\Http\Middleware;

use Closure;
use App\SubscriptionModel;
use Session;
use Config;
use Request;
use Redirect;
class Subscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 

        if (Request::header('msisdn','8801883001722')) {
            Session::set('msisdn', Request::header('msisdn','8801883001722'));
            return $next($request);
        }
        return Redirect::to('http://192.168.100.157/robitube/verify');
        
    }
}
