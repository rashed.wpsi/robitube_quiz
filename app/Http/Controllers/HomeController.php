<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Requests;
use Validator;
use Session;
use Redirect;
use Config;
use Illuminate\Http\Response;
use App\SubscriptionModel;
use App\ReplyModel;


class HomeController extends Controller
{

    public function index()
    {
    	if (Session::has('msisdn')) {
      	 Session::forget('msisdn');
      }
      return view('msisdn.msisdn');

    }
    /**
     * [check description]
     * @return [type] [description]
     */
    public function check()
    {
     $msisdn=Request::get('msisdn');
	    if (substr($msisdn,0,2)!=88) {
				$msisdn="88".Request::get('msisdn');
		}
		$data=['msisdn' => $msisdn];
		$validator = Validator::make($data,[
	    'msisdn' => 'required|digits_between:11,13|regex:/^\88018/'],['required'=>'Mobile number is required!','digits_between'=>'Mobile number must be between 11 to 13 digits!','regex'=>'Only Robi number is allowed!']);

		if ($validator->fails())
		{
		 return redirect('/quiz')->withErrors($validator);  
	    } 
	    $subscribe=SubscriptionModel::Subscriber($msisdn)->Status(1)->Keyword(Config::get('custom.keyword'))->Shortcode(Config::get('custom.shortcode'))->first();
	    $is_new=ReplyModel::MSISDN($msisdn)->Keyword(Config::get('custom.keyword'))->Shortcode(Config::get('custom.shortcode'))->Operator(Config::get('custom.operator'))->count();
	    
	    Session::set('msisdn',$msisdn);
	    if (count($subscribe)>0) {
	    	return redirect('rules');
	    }
     //  if (count($subscribe)>0 && $is_new<1) {	    
	    // 	return redirect('rules');
	    // }
	    // else{
	    // 	return redirect('question');
	    // }
	    return redirect('info_subs');
    }
	public function rules()
	{

		$title = 'Quiz Rules';
		$row = SubscriptionModel::where('msisdn', '=', Session::get('msisdn'))->
				where('status', '=', 1)->first();
				//dd($row);
	 	return view('quiz.description', compact('title', 'row'));

	}

}
