<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Session;
use DB;
use Config;
use Request;
use Validator;
use App\Http\Requests;
use App\QuestionModel;
use App\ReplyModel;
use App\WinnerModel;

class QuestionController extends Controller
{
    public function index()
    {
       //dd(Config::get('custom.max_limit'));
      $score=ReplyModel::Shortcode(Config::get('custom.shortcode'))->Keyword(Config::get('custom.keyword'))->MSISDN(Session::get('msisdn'))->Correct(1)->count();
      $wrong=ReplyModel::Shortcode(Config::get('custom.shortcode'))->Keyword(Config::get('custom.keyword'))->MSISDN(Session::get('msisdn'))->Correct(0)->count();
      $todayPlayed=ReplyModel::Shortcode(Config::get('custom.shortcode'))->Keyword(Config::get('custom.keyword'))->MSISDN(Session::get('msisdn'))->Today()->count();
      $question=$todayPlayed<=Config::get('custom.max_limit')?$this->get_questions():0;
      //dd($question);
      // dd($todayPlayed);
      return view('quiz.question',compact('score','wrong','todayPlayed','question'));
    }
    public function get_questions()
    {
        $ids=ReplyModel::Keyword(Config::get('custom.keyword'))->MSISDN(Session::get('msisdn'))->Shortcode(Config::get('custom.shortcode'))->Operator(Config::get('custom.operator'))->lists('quize_id');
        $question = QuestionModel::Keyword(Config::get('custom.keyword'))->Shortcode(Config::get('custom.shortcode'))->Operator(Config::get('custom.operator'))->Status(1)->whereNotIn('id',$ids)->first();
        return $question;
   
    }

    public function ques_check(){
    	$given_answer=Request::get('optradio');
    	//dd($given_answer);
    	$data=['optradio' => $given_answer];
    	$validator=Validator::make($data,['optradio'=>'required'],['optradio.required'=>'You need to select an answer & then press Next for  next question.']);
        if ($validator->fails())
		{
		 return redirect('/question')->withErrors($validator);  
	    } 
	    $answered=Request::get('optradio')==Request::get('answer')?1:0;
	    $to_be_save=[
	               'quize_id'   =>Request::get('quiz_id'),
	               'shortcode'  =>Config::get('custom.shortcode'),
	               'operator'   =>Config::get('custom.operator'),
	               'keyword'    =>Config::get('custom.keyword'),
	               'msisdn'     =>Session::get('msisdn'),
	               'answered'   =>Request::get('optradio'),
	               'is_correct' =>$answered
   	               ];
   	    $insert=new ReplyModel();
        $insert->inbox_id = Request::get('inbox_id');
        $insert->quize_id = Request::get('quiz_id');
        $insert->shortcode = Config::get('custom.shortcode');
        $insert->operator = Config::get('custom.operator');
        $insert->keyword = Config::get('custom.keyword');
        $insert->msisdn = Session::get('msisdn');
        $insert->answered = Request::get('optradio');
        $insert->is_correct = $answered;
        $insert->charged = 0;
        $insert->creation_time = date('Y-m-d H:i:s');
        $insert->save();
	    return redirect('/question');

    }
}
