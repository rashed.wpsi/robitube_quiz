<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $connection = 'sqlsrv1';
    protected $table = 'subscribers';
}
