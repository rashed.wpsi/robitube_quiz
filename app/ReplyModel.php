<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class ReplyModel extends Model
{
	public $timestamps = false;
	/**
	 * connection name
	 */
   // protected $connection='sqlsrv1';
	/**
	 * table name
	 * @var string
	 */
   protected $table = 'quiz_reply';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = ['quize_id','shortcode','keyword','operator','msisdn','answered','is_correct'];
   /**
    * 
    */
  const CREATED_AT = 'creation_time';

   public function scopeToday($query)
    {
     // dd(getdate());
       return $query->where( DB::raw('CONVERT(date, creation_time, 102)'), '=', DB::raw('CONVERT(date, getdate(), 102)'));
    }
   public function ScopeMSISDN($query,$msisdn)
   {
       return $query->where('msisdn',$msisdn);
   }
   public function ScopeKeyword($query,$keyword)
   {
       return $query->where('keyword',$keyword);
   }
   public function ScopeShortcode($query,$shortcode)
   {
       return $query->where('shortcode',$shortcode);
   }
   public function ScopeOperator($query,$operator)
   {
       return $query->where('operator',$operator);
   }
   public function ScopeCorrect($query,$correct)
   {
       
       return $query->where('is_correct',$correct);
       
   }
   public function getDateFormat()
    {
       return 'Y-m-d H:i:s.u';
    }

   public function fromDateTime($value)
    {
       return substr(parent::fromDateTime($value), 0, -3);
    }
}
