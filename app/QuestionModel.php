<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionModel extends Model
{
    /**
	 * connection name
	 */
   //protected $connection='sqlsrv1';
	/**
	 * table name
	 * @var string
	 */
   protected $table = 'quiz_bank';

   public function ScopeKeyword($query,$keyword)
   {
       return $query->where('keyword',$keyword);
   }
   public function ScopeShortcode($query,$shortcode)
   {
       return $query->where('shortcode',$shortcode);
   }
   public function ScopeOperator($query,$operator)
   {
       return $query->where('operator',$operator);
   }
   public function scopeStatus($query, $status)
   {
        if($status > 0) {
            return $query->where('status', '=', $status);
        }
   }
}
